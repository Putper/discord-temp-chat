import discord
import asyncio
import logging
import configparser
import json_helper


# Setup logging
logger = logging.getLogger('discord')
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)


# Initialise config
config = configparser.ConfigParser();
config.read('config.ini');
command = config.get('Bot', 'Command')
token = config.get('Bot', 'Token')


client = discord.Client()

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')
    

@client.event
async def on_message(message):
    # Help : returns commands with description
    if message.content.startswith(command + " help"):
        await client.send_message(message.channel, 
        '**' + command + ' help** - _bot commands_ \n' + 
        '**' + command + ' create <name>** - _Creates a channel with specified name_ \n' +
        '**' + command + ' remove** - _Removes your channel_')

    # Create channel
    if message.content.startswith(command + " create"):
        if not json_helper.user_has_channel(message.channel.server.id, message.author.id):    # if the user doesn't already have a channel
            name = message.content.replace(command + " create ", '')    # get parameter
            channel = await client.create_channel(message.channel.server, name) # create channel
            await asyncio.sleep(1)  # wait a bit
            json_helper.add_channel(channel.server.id, channel.id, message.author.id) # add channel to json file
            await client.send_message(message.channel, name + ' has been created.') # notify user that the server has been created
        else:   # if user already has a channel, notify him
            await client.send_message(message.channel, 'You already own a channel, please remove it before creating a new one.')


    # Remove channel
    if message.content.startswith(command + " remove"):
        entry = json_helper.remove_channel(message.channel.server.id, message.author.id)
        channel = client.get_channel( entry['channel_id'] )
        if(channel):
            await client.delete_channel(channel)
            await client.send_message(message.channel, 'Your channel has been removed.')


client.run(token)
